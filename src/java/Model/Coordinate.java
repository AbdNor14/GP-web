package Model;

public class Coordinate implements Cloneable
{
    private double x;
    private double y;
    private double z;

    public Coordinate()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    public Coordinate(Coordinate c)
    {
        this.x = c.getX();
        this.y = c.getY();
        this.z = c.getZ();
    }

    public Coordinate(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    private double[] cartisian2Spharical()
    {
        double res = squareOfSum(x, y);

        double r = squareOfSum(res, z);
        double elev = Math.atan2(z, res);
        double az = Math.atan2(y, x);

        return new double[] { az, elev, r };
    }

    @Override
    public Object clone() throws CloneNotSupportedException
    {
        return new Coordinate(new Double(x), new Double(y), new Double(z));
    }

    public int[] getVectorDirection()
    {
        double[] a = cartisian2Spharical();
        int      theta = (int) Math.round(((a[0] * 180) / Math.PI));
        int      phai = (int) Math.round(((a[1] * 180) / Math.PI));
        int      i = 0;

        if (theta < 0)
        {
            theta += 360;
        }

        if ((phai >= 0) && (phai <= 90))
        {
            if ((theta >= 0) && ((theta < 45) || (theta >= 315)))
            {
                i = 1;
            }
            else if ((theta >= 45) && (theta < 135))
            {
                i = 2;
            }
            else if ((theta >= 135) && (theta < 225))
            {
                i = 3;
            }
            else if ((theta >= 225) && (theta < 315))
            {
                i = 4;
            }
        }
        else if ((phai >= -90) && (phai <= 0))
        {
            if ((theta >= 0) && ((theta < 45) || (theta >= 315)))
            {
                i = 5;
            }
            else if ((theta >= 45) && (theta < 135))
            {
                i = 6;
            }
            else if ((theta >= 135) && (theta < 225))
            {
                i = 7;
            }
            else if ((theta >= 225) && (theta < 315))
            {
                i = 8;
            }
        }
        else
        {
            i = 9;
        }

        int r = (int) Math.round(a[2]);

        return new int[] { i, theta, phai, r };
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public double getZ()
    {
        return z;
    }

    public void setX(double x)
    {
        this.x = x;
    }

    public void setY(double y)
    {
        this.y = y;
    }

    public void setZ(double z)
    {
        this.z = z;
    }

    private static double squareOfSum(double x, double y)
    {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    public void substruct(Coordinate c1, Coordinate c2)
    {
        this.x = c1.getX() - c2.getX();
        this.y = c1.getY() - c2.getY();
        this.z = c1.getZ() - c2.getZ();
    }
    
    public double squareOfSum(){
        
        return Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2) ; 
    }
}
