/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Abdallah
 */
public class Sequence implements Cloneable
{

    private char chainId;
    private int numberOfResidue;
    private final ArrayList<String> sequnceTmp;
    private ArrayList<String> residueNames;
    private String sequence;

    public Sequence()
    {
        this.seqTable = new HashMap<String, Character>()
        {


            {
                put("ALA", 'A');
                put("ARG", 'R');
                put("ASN", 'N');
                put("ASP", 'D');
                put("ASX", 'B');
                put("CYS", 'C');
                put("GLN", 'Q');
                put("GLU", 'E');
                put("GLX", 'Z');
                put("GLY", 'G');
                put("HIS", 'H');
                put("ILE", 'I');
                put("LEU", 'L');
                put("LYS", 'K');
                put("MET", 'M');
                put("PHE", 'F');
                put("PRO", 'P');
                put("SER", 'S');
                put("THR", 'T');
                put("TRP", 'W');
                put("TYR", 'Y');
                put("VAL", 'V');
                put("UNK", 'X');

            }
        };
        sequnceTmp = new ArrayList<>();
        residueNames = new ArrayList<>();
        sequence = "";
        numberOfResidue = 0;
    }

    public Sequence(char chainId, int numberOfResidue,
                    ArrayList<String> sequnceTmp,
                    ArrayList<String> residueNames, String sequence)
    {
        this.seqTable =
        new HashMap<String, Character>()
        {


            {
                put("ALA", 'A');
                put("ARG", 'R');
                put("ASN", 'N');
                put("ASP", 'D');
                put("ASX", 'B');
                put("CYS", 'C');
                put("GLN", 'Q');
                put("GLU", 'E');
                put("GLX", 'Z');
                put("GLY", 'G');
                put("HIS", 'H');
                put("ILE", 'I');
                put("LEU", 'L');
                put("LYS", 'K');
                put("MET", 'M');
                put("PHE", 'F');
                put("PRO", 'P');
                put("SER", 'S');
                put("THR", 'T');
                put("TRP", 'W');
                put("TYR", 'Y');
                put("VAL", 'V');
                put("UNK", 'X');

            }
        };
        this.chainId = chainId;
        this.numberOfResidue = numberOfResidue;
        this.sequnceTmp = sequnceTmp;
        this.residueNames = residueNames;
        this.sequence = sequence;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new Sequence(chainId, numberOfResidue, new ArrayList<>(
                this.sequnceTmp), new ArrayList<>(this.residueNames),
                            sequence);
    }

    private final Map<String, Character> seqTable;

    public void extractSequence()
    {
        String seq = "";
        for (String line
             : this.sequnceTmp)
        {
            String[] items = line.split(" ");
            for (String item : items)
            {
                if (!item.isEmpty())
                {
                    Character value =
                              seqTable.
                            get(item.
                                    trim());
                    if (value !=
                        null)
                    {
                        seq += value;
                        residueNames.
                                add(item);
                    }
                    else
                    {
                        seq += '?';
                        residueNames.
                                add(item);
                    }
                }
            }
        }
        this.sequence = seq;
    }

    public void extract(String query)
    {
        if (!query.isEmpty())
        {
            chainId = query.
                    charAt(5);
            numberOfResidue =
            Integer.parseInt(query.substring(6, 11).
                    trim());
            sequnceTmp.add(query.
                    substring(12));
        }
    }

    public void append(String query)
    {
        if (!query.isEmpty())
        {
            sequnceTmp.add(query.
                    substring(12));
        }
    }

    public char getChainId()
    {
        return chainId;
    }

    public void setChainId(
            char chainId)
    {
        this.chainId = chainId;
    }

    public int getNumberOfResidue()
    {
        return numberOfResidue;
    }

    public void setNumberOfResidue(int numberOfResidue)
    {
        this.numberOfResidue = numberOfResidue;
    }

    public ArrayList<String> getResidueNames()
    {
        return residueNames;
    }

    public void setResidueNames(ArrayList<String> residueNames)
    {
        this.residueNames = residueNames;
    }

    public String getSequence()
    {
        return sequence;
    }

    public void setSequence(String sequence)
    {
        this.sequence = sequence;
    }
}
