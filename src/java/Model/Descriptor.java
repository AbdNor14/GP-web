/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.ArrayList;

/**
 * @author Toshiba
 */
public class Descriptor implements Cloneable
{

    private String types;
    private ArrayList<Integer> resultantsSymbols;
    private ArrayList<Integer> resultantsTheta;
    private ArrayList<Integer> resultantsPhi;
    private ArrayList<Integer> interdirectionsSymbols;
    private ArrayList<Integer> interdirectionsTheta;
    private ArrayList<Integer> interdirectionsPhi;
    private ArrayList<Integer> proximity;

    public Descriptor()
    {
        this.types = "";
        this.resultantsSymbols = new ArrayList<>();
        this.resultantsTheta = new ArrayList<>();
        this.resultantsPhi = new ArrayList<>();
        this.interdirectionsSymbols = new ArrayList<>();
        this.interdirectionsTheta = new ArrayList<>();
        this.interdirectionsPhi = new ArrayList<>();
        this.proximity = new ArrayList<>();
    }

    public Descriptor(String types, ArrayList<Integer> resultantsSymbols,
                      ArrayList<Integer> resultantsTheta,
                      ArrayList<Integer> resultantsPhi,
                      ArrayList<Integer> interdirectionsSymbols,
                      ArrayList<Integer> interdirectionsTheta,
                      ArrayList<Integer> interdirectionsPhi,
                      ArrayList<Integer> proximity)
    {
        this.types = types;
        this.resultantsSymbols = resultantsSymbols;
        this.resultantsTheta = resultantsTheta;
        this.resultantsPhi = resultantsPhi;
        this.interdirectionsSymbols = interdirectionsSymbols;
        this.interdirectionsTheta = interdirectionsTheta;
        this.interdirectionsPhi = interdirectionsPhi;
        this.proximity = proximity;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new Descriptor(types, new ArrayList<>(
                this.resultantsSymbols),
                              new ArrayList<>(this.resultantsTheta),
                              new ArrayList<>(this.resultantsPhi),
                              new ArrayList<>(this.interdirectionsSymbols),
                              new ArrayList<>(this.interdirectionsTheta),
                              new ArrayList<>(this.interdirectionsPhi),
                              new ArrayList<>(this.proximity));
    }

    public String getTypes()
    {
        return types;
    }

    public void setTypes(String types)
    {
        this.types = types;
    }

    public ArrayList<Integer> getResultantsSymbols()
    {
        return resultantsSymbols;
    }

    public void setResultantsSymbols(ArrayList<Integer> resultantsSymbols)
    {
        this.resultantsSymbols = resultantsSymbols;
    }

    public ArrayList<Integer> getResultantsTheta()
    {
        return resultantsTheta;
    }

    public void setResultantsTheta(ArrayList<Integer> resultantsTheta)
    {
        this.resultantsTheta = resultantsTheta;
    }

    public ArrayList<Integer> getResultantsPhi()
    {
        return resultantsPhi;
    }

    public void setResultantsPhi(ArrayList<Integer> resultantsPhi)
    {
        this.resultantsPhi = resultantsPhi;
    }

    public ArrayList<Integer> getInterdirectionsSymbols()
    {
        return interdirectionsSymbols;
    }

    public void setInterdirectionsSymbols(
            ArrayList<Integer> interdirectionsSymbols)
    {
        this.interdirectionsSymbols = interdirectionsSymbols;
    }

    public ArrayList<Integer> getInterdirectionsTheta()
    {
        return interdirectionsTheta;
    }

    public void setInterdirectionsTheta(ArrayList<Integer> interdirectionsTheta)
    {
        this.interdirectionsTheta = interdirectionsTheta;
    }

    public ArrayList<Integer> getInterdirectionsPhi()
    {
        return interdirectionsPhi;
    }

    public void setInterdirectionsPhi(ArrayList<Integer> interdirectionsPhi)
    {
        this.interdirectionsPhi = interdirectionsPhi;
    }

    public ArrayList<Integer> getProximity()
    {
        return proximity;
    }

    public void setProximity(ArrayList<Integer> proximity)
    {
        this.proximity = proximity;
    }

}
