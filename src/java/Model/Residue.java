package Model;


/**
 * @author Abdallah
 */
public class Residue implements Cloneable
{
    protected String residueName;
    protected char   chainIdentifier;
    protected char   codeForInsertionsOfResidues;
    protected int    residueSequenceNumber;

    /**
     *
     */
    public Residue()
    {
    }

    /**
     * @param residueName
     * @param identifier
     * @param residueSequenceNumber
     * @param codeForInsertionsOfResidues
     */
    public Residue(String residueName, char identifier,
                   int residueSequenceNumber, char codeForInsertionsOfResidues)
    {
        this.residueName = residueName;
        this.chainIdentifier = identifier;
        this.residueSequenceNumber = residueSequenceNumber;
        this.codeForInsertionsOfResidues = codeForInsertionsOfResidues;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new Residue(residueName, chainIdentifier, residueSequenceNumber,
                           codeForInsertionsOfResidues); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return
     */
    public char getChainIdentifier()
    {
        return chainIdentifier;
    }

    /**
     * @return
     */
    public char getCodeForInsertionsOfResidues()
    {
        return codeForInsertionsOfResidues;
    }

    /**
     * @return
     */
    public String getResidueName()
    {
        return residueName;
    }

    /**
     * @return
     */
    public int getResidueSequenceNumber()
    {
        return residueSequenceNumber;
    }

    /**
     * @param chainIdentifier
     */
    public void setChainIdentifier(char chainIdentifier)
    {
        this.chainIdentifier = chainIdentifier;
    }

    /**
     * @param codeForInsertionsOfResidues
     */
    public void setCodeForInsertionsOfResidues(char codeForInsertionsOfResidues)
    {
        this.codeForInsertionsOfResidues = codeForInsertionsOfResidues;
    }

    /**
     * @param residueName
     */
    public void setResidueName(String residueName)
    {
        this.residueName = residueName;
    }

    /**
     * @param residueSequenceNumber
     */
    public void setResidueSequenceNumber(int residueSequenceNumber)
    {
        this.residueSequenceNumber = residueSequenceNumber;
    }
}
