package Model;


/**
 * @author uranus
 */
public class Atom implements Cloneable
{
    private Coordinate orthogonal;
    private String     atomName;
    private char       chainID;
    private int        atomSerialNumber;
    private int        residueSequenceNumber;

    public Atom(int atomSerialNumber, int residueSequenceNumber,
                String atomName, Coordinate orthogonal, char chainID)
    {
        this.atomSerialNumber = atomSerialNumber;
        this.residueSequenceNumber = residueSequenceNumber;
        this.atomName = atomName;
        this.orthogonal = orthogonal;
        this.chainID = chainID;
    }

    public Atom()
    {
        orthogonal = new Coordinate();
        chainID = 0;
    }

    public Atom(int residuNumber)
    {
        this.residueSequenceNumber = residuNumber;
    }

    public Atom(char chID)
    {
        this.chainID = chID;
    }

    public Atom(int residuNumber, char chID)
    {
        this.chainID = chID;
        this.residueSequenceNumber = residuNumber;
    }

    public void extract(String string)
    {
        setAtomSerialNumber(Integer.parseInt(string.substring(6, 11).trim()));
        setResidueSequenceNumber(Integer.parseInt(string.substring(22, 26).trim()));
        setAtomName(string.substring(12, 16));
        setOrthogonal(new Coordinate(Double.parseDouble(string.substring(30, 38)
                                                           .trim()),
                                     Double.parseDouble(string.substring(38, 46)
                                                           .trim()),
                                     Double.parseDouble(string.substring(46, 54)
                                                           .trim())));
        setChainID(string.charAt(21));
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new Atom(atomSerialNumber, residueSequenceNumber, atomName,
                        new Coordinate(orthogonal.getX(), orthogonal.getY(),
                                       orthogonal.getZ()), chainID);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (Atom.class == obj.getClass())
        {
            Atom at = (Atom) obj;

            if ((this.chainID != 0) &&
                    ((at.getChainID() == chainID) &&
                        (residueSequenceNumber == at.getResidueSequenceNumber())))
            {
                return true;
            }
        }

        return false;
    }

    public String getAtomName()
    {
        return atomName;
    }

    public int getAtomSerialNumber()
    {
        return atomSerialNumber;
    }

    public char getChainID()
    {
        return chainID;
    }

    public Coordinate getOrthogonal()
    {
        return orthogonal;
    }

    public int getResidueSequenceNumber()
    {
        return residueSequenceNumber;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = (59 * hash) + this.chainID;
        hash = (59 * hash) + this.residueSequenceNumber;

        return hash;
    }

    public void setAtomName(String atomName)
    {
        this.atomName = atomName;
    }

    public void setAtomSerialNumber(int atomSerialNumber)
    {
        this.atomSerialNumber = atomSerialNumber;
    }

    public void setChainID(char chainID)
    {
        this.chainID = chainID;
    }

    public void setOrthogonal(Coordinate orthogonal)
    {
        this.orthogonal = orthogonal;
    }

    public void setResidueSequenceNumber(int residueSequenceNumber)
    {
        this.residueSequenceNumber = residueSequenceNumber;
    }
}
