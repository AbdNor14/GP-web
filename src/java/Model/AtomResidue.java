package Model;


/**
 * @author Abdallah
 */
public class AtomResidue extends Residue implements Cloneable
{
    private String atomName;

    /**
     * @param atomName
     * @param residueName
     * @param identifier
     * @param residueSequenceNumber
     * @param codeForInsertionsOfResidues
     */
    public AtomResidue(String atomName, String residueName, char identifier,
                       int residueSequenceNumber,
                       char codeForInsertionsOfResidues)
    {
        super(residueName, identifier, residueSequenceNumber,
              codeForInsertionsOfResidues);
        this.atomName = atomName;
    }

    /**
     *
     */
    public AtomResidue()
    {
        super();
    }

    @Override
    protected Object clone() throws CloneNotSupportedException
    {
        return new AtomResidue(atomName, residueName, chainIdentifier,
                               residueSequenceNumber,
                               codeForInsertionsOfResidues); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return
     */
    public String getAtomName()
    {
        return atomName;
    }

    /**
     * @param atomName
     */
    public void setAtomName(String atomName)
    {
        this.atomName = atomName;
    }
}
