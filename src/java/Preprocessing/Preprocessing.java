/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preprocessing;

import Model.Atom;
import Model.Helix;
import Model.Protein;
import Model.SecondaryStructure;
import Model.Sequence;
import Model.Sheet;
import java.util.ArrayList;

/**
 *
 * @author Abdallah
 */
public class Preprocessing
{

    private ArrayList<SecondaryStructure> helicesObjects;
    private ArrayList<SecondaryStructure> sheetsObjects;
    private ArrayList<Atom> atomsObjects;
    private ArrayList<Sequence> sequencesObjects;
    private Protein protein;

    public Preprocessing()
    {
        this.helicesObjects = new ArrayList<>();
        this.sheetsObjects = new ArrayList<>();
        this.atomsObjects = new ArrayList<>();
        this.sequencesObjects = new ArrayList<>();
        this.protein = new Protein();
    }

    public ArrayList<SecondaryStructure> getHelicesObjects()
    {
        return helicesObjects;
    }

    public void setHelicesObjects(
            ArrayList<SecondaryStructure> helicesObjects)
    {
        this.helicesObjects = helicesObjects;
    }

    public ArrayList<SecondaryStructure> getSheetsObjects()
    {
        return sheetsObjects;
    }

    public void setSheetsObjects(
            ArrayList<SecondaryStructure> sheetsObjects)
    {
        this.sheetsObjects = sheetsObjects;
    }

    public ArrayList<Atom> getAtomsObjects()
    {
        return atomsObjects;
    }

    public void setAtomsObjects(ArrayList<Atom> atomsObjects)
    {
        this.atomsObjects = atomsObjects;
    }

    public ArrayList<Sequence> getSequencesObjects()
    {
        return sequencesObjects;
    }

    public void setSequencesObjects(
            ArrayList<Sequence> sequencesObjects)
    {
        this.sequencesObjects = sequencesObjects;
    }

    public Protein getProtein()
    {
        return protein;
    }

    public void setProtein(Protein protein)
    {
        this.protein = protein;
    }

    /*
     * Read from protein file - "filename.PDB" - the helices ,sheets , atoms and
     * sequences data as strings and store it in Arraylists in Controller class
     *
     * @param filename PDB File name
     * @throws java.lang.CloneNotSupportedException
     */
    public void readFile(String filename) throws Exception
    {

        PDBReader pdbreader = new PDBReader(filename);
        pdbreader.readPDB();
        this.protein.setProteinId(pdbreader.getID());
        ArrayList<String> helicesStrings = pdbreader.getHelix();
        ArrayList<String> sheetsStrings = pdbreader.getSheet();
        ArrayList<String> atomsStrings = pdbreader.getAtom();
        ArrayList<String> sequencesStrings = pdbreader.getSequence();

        for (String helixString : helicesStrings)
        {
            Helix helix;
            helix = new Helix();
            helix.extract(helixString);
            helicesObjects.add(helix);
        }

        for (String sheetString : sheetsStrings)
        {
            Sheet sheet;
            sheet = new Sheet();
            sheet.extract(sheetString);
            sheetsObjects.add(sheet);
        }

        for (String atomString : atomsStrings)
        {
            Atom atom;
            atom = new Atom();
            atom.extract(atomString);
            atomsObjects.add(atom);
        }

        String prevSequences = "";
        Sequence sequence = null;
        for (String sequenceString : sequencesStrings)
        {
            /**
             * Because the sequences of the same id written in multiple line in
             * the file we need to gather it in one object ..
             */
            String sequenceId = sequenceString.substring(5, 6);
            if (prevSequences.contains(sequenceId))
            {
                assert sequence != null;
                sequence.append(sequenceString);
            } else
            {
                if (sequence != null)
                {
                    sequence.extractSequence();
                    sequencesObjects.add(sequence);
                }
                sequence = new Sequence();
                sequence.extract(sequenceString);
                prevSequences += sequenceId;
            }
        }
        sequencesObjects.add(sequence);
    }

    public void preprocess(String fileName) throws Exception
    {
        readFile(fileName);
        protein.process(atomsObjects, helicesObjects, sheetsObjects,
                sequencesObjects);
    }

    public void preprocessAndWrite(String fileName) throws Exception
    {
        readFile(fileName);
        protein.process(atomsObjects, helicesObjects, sheetsObjects,
                sequencesObjects);
        Protein cleaned = protein.clean();
        if (cleaned.getNumberOfChains() > 0)
        {
            writeToFile(cleaned, fileName);
        }else{
            throw new Exception("Passed preprocess , but accepted chain = 0!");
        }
    }

    public void writeToFile(Protein protein, String fileName)
    {
        ProteinWriter pw = new ProteinWriter();
        String split = fileName.substring(0, fileName.lastIndexOf('.'));
        pw.write(protein, split + ".txt");
    }
}
