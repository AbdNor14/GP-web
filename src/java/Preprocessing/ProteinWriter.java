/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Preprocessing;

import Model.Chain;
import Model.SSE;
import Model.Coordinate;
import Model.Protein;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Abdallah
 */
public class ProteinWriter
{

    public void write(Protein protein, String fileName)
    {
        try
        {
            String newLine = "\r\n";
            String space = " ";
            try (FileWriter fw = new FileWriter(fileName))
            {
                try (BufferedWriter bw = new BufferedWriter(fw))
                {
                    bw.write(protein.getProteinId() + newLine);
                    bw.write(protein.getListOfChains().size() + newLine);
                    for (Chain ch : protein.getListOfChains())
                    {
                        bw.write(ch.getChainId() + newLine);
                        bw.write(ch.getDescriptor().getTypes() + newLine);
                        for (int n : ch.getDescriptor().getResultantsSymbols())
                        {
                            bw.write(n + space);
                        }
                        bw.write(newLine);

                        for (int n : ch.getDescriptor().getResultantsTheta())
                        {
                            bw.write(n + space);
                        }
                        bw.write(newLine);

                        for (int n : ch.getDescriptor().getResultantsPhi())
                        {
                            bw.write(n + space);
                        }
                        bw.write(newLine);

                        /**
                         * This (if) statement ,because the number of Resultants
                         * exceed Interdirections by one, so no need to write
                         * extra new lines in the file.
                         */
                        if (ch.getDescriptor().getResultantsPhi().size() > 1)
                        {
                            for (int n : ch.getDescriptor().
                                    getInterdirectionsSymbols())
                            {
                                bw.write(n + space);
                            }
                            bw.write(newLine);

                            for (int n : ch.getDescriptor().
                                    getInterdirectionsTheta())
                            {
                                bw.write(n + space);
                            }
                            bw.write(newLine);

                            for (int n : ch.getDescriptor().
                                    getInterdirectionsPhi())
                            {
                                bw.write(n + space);
                            }
                            bw.write(newLine);

                            for (int n : ch.getDescriptor().getProximity())
                            {
                                bw.write(n + space);
                            }
                            bw.write(newLine);
                        }

                        bw.write(ch.getSseVector().size() + newLine);

                        String delim = "#";
                        for (SSE s : ch.getSseVector())
                        {
                            bw.write(s.getChainIndex() + newLine);
                            bw.write(s.getIndex() + newLine);
                            bw.write(s.getIntialIndex() + newLine);
                            bw.write(s.getEndIndex() + newLine);
                            bw.write(format(s.getCenteroid().getX()) + space +
                                    format(s.getCenteroid().getY()) + space +
                                    format(s.getCenteroid().getZ()) + newLine);
                            bw.write(format(s.getResultant().getX()) + space +
                                    format(s.getResultant().getY()) + space +
                                    format(s.getResultant().getZ()) + newLine);
                            bw.write(s.getSseClass() + newLine);
                        }

                        for (int index : ch.getResidueIndex())
                        {
                            bw.write(index + space);
                        }
                        bw.write(newLine);

                        for (Coordinate c : ch.getResiduePointCA())
                        {
                            bw.write(format(c.getX()) + space);
                            bw.write(format(c.getY()) + space);
                            bw.write(format(c.getZ()) + delim);
                        }
                        bw.write(newLine);

                    }
                    bw.flush();
                    bw.close();
                }
                fw.close();
            }
        } catch (IOException ex)
        {
            Logger.getLogger(ProteinWriter.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }

    public static String format(double n)
    {
        NumberFormat format = new DecimalFormat("#.##;-#");
        try
        {
            format.setMinimumFractionDigits(0);
            format.setMaximumFractionDigits(2);
        } catch (Exception ex)
        {
        }
        return format.format(n);
    }
}
