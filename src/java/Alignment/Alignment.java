/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignment;

import Model.Protein;
import java.util.ArrayList;

/**
 *
 * @author Abdallah
 */
public class Alignment
{

    private ProteinPermutator permutator;
    private ArrayList<Protein> permuteProtein24;
    private DescriptorComparor descriptorComparor;
    private double lrms;

    public double getLrms()
    {
        return lrms;
    }

    public void setLrms(double lrms)
    {
        this.lrms = lrms;
    }

    public ProteinPermutator getPermutator()
    {
        return permutator;
    }

    public void setPermutator(ProteinPermutator permutator)
    {
        this.permutator = permutator;
    }

    public ArrayList<Protein> getPermuteProtein24()
    {
        return permuteProtein24;
    }

    public void setPermuteProtein24(
            ArrayList<Protein> permuteProtein24)
    {
        this.permuteProtein24 = permuteProtein24;
    }

    public DescriptorComparor getDescriptorComparor()
    {
        return descriptorComparor;
    }

    public void setDescriptorComparor(
            DescriptorComparor descriptorComparor)
    {
        this.descriptorComparor = descriptorComparor;
    }

    /**
     *
     * @param queryFile
     * @param targetFile
     * @throws java.lang.Exception
     */
    public ArrayList<Object> align(String queryFile, String targetFile) throws Exception
    {
        ProteinReader queryReader = new ProteinReader();
        queryReader.ReaderFun(queryFile);
        Protein query = queryReader.getProtein();

        ProteinReader tragetReader = new ProteinReader();
        tragetReader.ReaderFun(targetFile);
        Protein target = tragetReader.getProtein();

        permutator = new ProteinPermutator(query);
        permuteProtein24 = permutator.permuteProtein();

        descriptorComparor = new DescriptorComparor();
        descriptorComparor.intialAllignment(permuteProtein24, target,
                permuteProtein24.get(0).getListOfChains().get(0).getChainId(),
                target.getListOfChains().get(0).getChainId());
        int[][] res = descriptorComparor.getMaxScores();
        ArrayList<Integer> calculateSSEMatchs;
        calculateSSEMatchs = descriptorComparor.calculateSSEMatchs(
                descriptorComparor.getBestRotaion(),
                target.getListOfChains().get(0), res);
        return descriptorComparor.calculateFinalAlignment(
                descriptorComparor.getBestRotaion(),
                target.getListOfChains().get(0), calculateSSEMatchs,
                descriptorComparor.getBestRotaionNumber(), 3);
    }
}
