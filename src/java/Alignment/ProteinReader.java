/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignment;

import Model.Chain;
import Model.Coordinate;
import Model.Protein;
import Model.SSE;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @author ahmed
 */
public class ProteinReader
{

    private Protein protein;

    public ProteinReader()
    {
        protein = new Protein();
    }

    public ProteinReader(Protein protein)
    {
        this.protein = protein;
    }

    private ArrayList<Coordinate> getPointsCA(String readLine)
    {
        String del = "#";
        String[] data = readLine.split(del);

        ArrayList<Coordinate> coordinates = new ArrayList<>();

        for (String coordinate : data)
        {
            coordinates.add(getCoordinate(coordinate));
        }

        return coordinates;
    }

    public Protein getProtein()
    {
        return protein;
    }

    public void setProtein(Protein protein)
    {
        this.protein = protein;
    }

    private ArrayList<Integer> getInt(String stringLine)
    {
        ArrayList<Integer> intVector = new ArrayList<>();
        String[] stringArray = stringLine.split(" ");
        for (String stringArray1 : stringArray)
        {
            intVector.add(Integer.parseInt(stringArray1));
        }
        return intVector;
    }

    private ArrayList<Double> getDouble(String stringLine)
    {

        ArrayList<Double> doubleVector = new ArrayList<>();
        String[] stringArray = stringLine.split(" ");
        for (String stringArray1 : stringArray)
        {
            doubleVector.add(Double.parseDouble(stringArray1));
        }
        return doubleVector;
    }

    private Coordinate getCoordinate(String stringLine)
    {

        String[] stringArray = stringLine.split(" ");
        Coordinate c = new Coordinate();
        c.setX(Double.parseDouble(stringArray[0]));
        c.setY(Double.parseDouble(stringArray[1]));
        c.setZ(Double.parseDouble(stringArray[2]));

        return c;
    }

    public void ReaderFun(String fileName) throws Exception
    {
        String s;
        try (FileReader fr = new FileReader(fileName))
        {
            try (BufferedReader br = new BufferedReader(fr))
            {
                while ((s = br.readLine()) != null)
                {
                    protein.setProteinId(s);
                    ArrayList<Chain> chainVector = new ArrayList<>();
                    String stypes;
                    int numOfChains = Integer.parseInt(br.readLine());
                    protein.setNumberOfChains(numOfChains);
                    for (int i = 0; i < numOfChains; i++)
                    {
                        Chain ch = new Chain();
                        ch.setChainId(br.readLine().charAt(0));
                        stypes = br.readLine();
                        ch.getDescriptor().setTypes(stypes);

                        ch.getDescriptor().setResultantsSymbols(getInt(br.
                                readLine()));
                        ch.getDescriptor().setResultantsTheta(getInt(br.
                                readLine()));
                        ch.getDescriptor().setResultantsPhi(
                                getInt(br.readLine()));
                        if (ch.getDescriptor().getResultantsPhi().size() > 1)
                        {
                            ch.getDescriptor().setInterdirectionsSymbols(getInt(
                                    br.readLine()));
                            ch.getDescriptor().setInterdirectionsTheta(getInt(
                                    br.readLine()));
                            ch.getDescriptor().setInterdirectionsPhi(getInt(br.
                                    readLine()));
                            ch.getDescriptor().setProximity(
                                    getInt(br.readLine()));
                        }
                        
                        ArrayList<SSE> sseVector = new ArrayList<>();
                        int numOfSSE = Integer.parseInt(br.readLine());
                        for (int j = 0; j < numOfSSE; j++)
                        {
                            SSE sse = new SSE();
                            sse.setChainIndex(Integer.parseInt(br.readLine()));
                            sse.setIndex(Integer.parseInt(br.readLine()));
                            sse.setIntialIndex(Integer.parseInt(br.readLine()));
                            sse.setEndIndex(Integer.parseInt(br.readLine()));
                            sse.setCenteroid(getCoordinate(br.readLine()));
                            sse.setResultant(getCoordinate(br.readLine()));
                            sse.setSseClass(Integer.parseInt(br.readLine()));
                            sse.setSseType(stypes.charAt(j));
                            sseVector.add(sse);
                        }

                        ch.setResidueIndex(getResidueIndex(br.readLine()));
                        ch.setResiduePointCA(getPointsCA(br.readLine()));
                        ch.setSseVector(sseVector);
                        chainVector.add(ch);
                    }
                    protein.setListOfChains(chainVector);
                }
            }catch (NumberFormatException exception){
                throw new Exception("Protein File read failed!");
            }
        }catch(IOException exception){
            throw new Exception("Protein File read failed!");
        }
    }

    private ArrayList<Integer> getResidueIndex(String readLine)
    {
        String[] data = readLine.split(" ");
        ArrayList<Integer> indecies = new ArrayList<>();

        for (String integer : data)
        {
            indecies.add(Integer.parseInt(integer));
        }

        return indecies;
    }
}
