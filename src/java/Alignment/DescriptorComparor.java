/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Alignment;

import Model.Chain;
import Model.Coordinate;
import Model.Descriptor;
import Model.Matrix;
import Model.Protein;
import Preprocessing.ProteinWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.jblas.DoubleMatrix;
import org.jblas.Singular;

/**
 * @author uranus
 */
public class DescriptorComparor
{

    private Chain bestRotaion;
    private int[][] maxScores;
    private int alignmentScore;
    private int bestRotaionNumber;
    private StringBuilder alignmentString;

    public DescriptorComparor()
    {
        alignmentScore = 10000;
    }

    public String getAlignmentString()
    {
        return alignmentString.toString();
    }

    private static int[][] calculateInitialAlignment(Chain pc1, Chain pc2)
    {
        Descriptor pcd1 = pc1.getDescriptor();
        Descriptor pcd2 = pc2.getDescriptor();
        int[][] Scores;
        int cost = 1;
        int n1 = pcd1.getTypes().length() - 1;
        int n2 = pcd2.getTypes().length() - 1;
        Scores = new int[n1 + 1][n2 + 1];

        int min;
        for (int i = 0; i < n1; i++)
        {
            Scores[i + 1][0] = Scores[i][0] + cost;
        }

        for (int j = 0; j < n2; j++)
        {
            Scores[0][j + 1] = Scores[0][j] + cost;
        }

        for (int i = 0; i < n1; i++)
        {
            for (int j = 0; j < n2; j++)
            {
                if ((pcd1.getTypes().charAt(i) == pcd2.getTypes().charAt(j)) &&
                         (pcd1.getResultantsSymbols().get(i) ==
                         pcd2.getResultantsSymbols().get(j)) &&
                         (pcd1.getInterdirectionsSymbols().get(i) ==
                         pcd2.getInterdirectionsSymbols().get(j)))
                {
                    Scores[i + 1][j + 1] = Scores[i][j];
                } else
                {
                    min = Math.min(Scores[i][j] + cost, Scores[i + 1][j] + cost);
                    Scores[i + 1][j + 1] = Math.
                            min(min, Scores[i][j + 1] + cost);
                }
            }
        }
        return Scores;
    }

    private void compareProteinDescriptors(ArrayList<Protein> p1, Protein p2,
            char c1, char c2)
    {
        Chain chain2 = p2.getSpecificChain(c2);
        Chain chain1 = p1.get(0).getSpecificChain(c1);

        int[][] Scores;
        int n1 = chain1.getDescriptor().getTypes().length() - 1;
        int n2 = chain2.getDescriptor().getTypes().length() - 1;

        for (int i = 0; i < 24; i++)
        {
            chain1 = p1.get(i).getSpecificChain(c1);
            Scores = calculateInitialAlignment(chain1, chain2);
            if (alignmentScore > Scores[n1][n2])
            {
                setAlignmentScore(Scores[n1][n2]);
                setMaxScores(Scores);
                setBestRotaion(chain1);
                setBestRotaionNumber(i);
            }
        }
    }

    public int getAlignmentScore()
    {
        return alignmentScore;
    }

    public Chain getBestRotaion()
    {
        return bestRotaion;
    }

    public int getBestRotaionNumber()
    {
        return bestRotaionNumber;
    }

    public int[][] getMaxScores()
    {
        return maxScores;
    }

    public void intialAllignment(ArrayList<Protein> p1, Protein p2, char c1,
            char c2)
    {
        if ((p2.getChainIndex(c2) != -1) && (p1.get(0).getChainIndex(c1) != -1))
        {
            compareProteinDescriptors(p1, p2, c1, c2);
        }
    }

    public void setAlignmentScore(int AlignmentScore)
    {
        this.alignmentScore = AlignmentScore;
    }

    public void setBestRotaion(Chain BestRotaion)
    {
        this.bestRotaion = BestRotaion;
    }

    public void setBestRotaionNumber(int BestRotaionNumber)
    {
        this.bestRotaionNumber = BestRotaionNumber;
    }

    public void setMaxScores(int[][] MaxScores)
    {
        this.maxScores = MaxScores;
    }

    public ArrayList<Integer> calculateSSEMatchs(Chain chain1, Chain chain2,
            int[][] max) throws Exception
    {
        return calculateSSEMatchs(chain1, chain2, max, false);
    }

    public ArrayList<Integer> calculateSSEMatchs(Chain chain1, Chain chain2,
            int[][] max, boolean check) throws Exception
    {
        //algin string
        alignmentString = new StringBuilder();
        StringBuilder alTypes1 = new StringBuilder();
        StringBuilder alAresultantsSymbols1 = new StringBuilder();
        StringBuilder alAinterdirectionsSymbols1 = new StringBuilder();
        StringBuilder alTypes2 = new StringBuilder();
        StringBuilder alAresultantsSymbols2 = new StringBuilder();
        StringBuilder alAinterdirectionsSymbols2 = new StringBuilder();
        StringBuilder alignmentResult = new StringBuilder();
        int numAlign = 0, numNotAlign = 0;

        Descriptor pcd1 = chain1.getDescriptor();
        Descriptor pcd2 = chain2.getDescriptor();
        int n1 = pcd1.getTypes().length() - 1;
        int n2 = pcd2.getTypes().length() - 1;
        ArrayList<Integer> SSeMatchs = new ArrayList<>(n1 + 1);
        for (int i = 0; i < n1 + 1; i++)
        {
            SSeMatchs.add(-1);
        }

        int i = n1, j = n2;

        while (true)
        {
            if (i == 0 && j == 0)
            {
                break;
            } else if (i == 0 && j > 0)
            {
                j--;
            } else if (i > 0 && j == 0)
            {
                i--;
            } else
            {
                if (max[i - 1][j] < max[i - 1][j - 1])
                {
                    alTypes1.insert(0, pcd1.getTypes().charAt(i - 1));
                    alAresultantsSymbols1.insert(0, pcd1.getResultantsSymbols().
                            get(
                                    i - 1));
                    alAinterdirectionsSymbols1.insert(0, pcd1.
                            getInterdirectionsSymbols().get(i - 1));
                    alTypes2.insert(0, '-');
                    alAresultantsSymbols2.insert(0, '-');
                    alAinterdirectionsSymbols2.insert(0, '-');
                    alignmentResult.insert(0, '-');
                    numNotAlign++;
                    i--;
                } else if (max[i][j - 1] < max[i - 1][j - 1])
                {
                    alTypes1.insert(0, '-');
                    alAresultantsSymbols1.insert(0, '-');
                    alAinterdirectionsSymbols1.insert(0, '-');
                    alTypes2.insert(0, pcd2.getTypes().charAt(j - 1));
                    alAresultantsSymbols2.insert(0, pcd2.getResultantsSymbols().
                            get(j - 1));
                    alAinterdirectionsSymbols2.insert(0, pcd2.
                            getInterdirectionsSymbols().get(j - 1));
                    alignmentResult.insert(0, '-');
                    numNotAlign++;
                    j--;
                } else
                {
                    if ((pcd1.getTypes().charAt(i - 1) ==
                             pcd2.getTypes().charAt(j - 1)) &&
                             (pcd1.getResultantsSymbols().get(i - 1) ==
                             pcd2.getResultantsSymbols().get(j - 1)) &&
                             (pcd1.getInterdirectionsSymbols().get(i - 1) ==
                             pcd2.getInterdirectionsSymbols().get(j - 1)))
                    {
                        alignmentResult.insert(0, '|');
                        numAlign++;
                        SSeMatchs.set(i - 1, j - 1);
                    } else
                    {
                        alignmentResult.insert(0, '.');
                        numNotAlign++;
                    }

                    alTypes1.insert(0, pcd1.getTypes().charAt(i - 1));
                    alAresultantsSymbols1.insert(0, pcd1.getResultantsSymbols().
                            get(i - 1));
                    alAinterdirectionsSymbols1.insert(0, pcd1.
                            getInterdirectionsSymbols().get(i - 1));
                    alTypes2.insert(0, pcd2.getTypes().charAt(j - 1));
                    alAresultantsSymbols2.insert(0, pcd2.getResultantsSymbols().
                            get(j - 1));
                    alAinterdirectionsSymbols2.insert(0, pcd2.
                            getInterdirectionsSymbols().get(j - 1));

                    i--;
                    j--;
                }
            }
        }
        if (check)
        {
            double similarityScore;
            j = 0;
            DoubleMatrix pc1Centroids = DoubleMatrix.zeros(3, numAlign);
            DoubleMatrix pc2Centroids = DoubleMatrix.zeros(3, numAlign);

            for (int k = 0; k < n1; k++)
            {
                if (SSeMatchs.get(k) != 0)
                {
                    pc1Centroids.put(0, j, chain1.getSseVector().get(k).
                            getCenteroid().getX());
                    pc1Centroids.put(1, j, chain1.getSseVector().get(k).
                            getCenteroid().getY());
                    pc1Centroids.put(2, j, chain1.getSseVector().get(k).
                            getCenteroid().getZ());
                    int sseIndex = SSeMatchs.get(k);
                    pc2Centroids.put(0, j, chain2.getSseVector().get(sseIndex).
                            getCenteroid().getX());
                    pc2Centroids.put(1, j, chain2.getSseVector().get(sseIndex).
                            getCenteroid().getY());
                    pc2Centroids.put(2, j, chain2.getSseVector().get(sseIndex).
                            getCenteroid().getZ());
                    j++;
                }
            }

            ArrayList<Object> result = kabsch(pc1Centroids, pc2Centroids);
            similarityScore = (double) numAlign / Math.min(n1, n2) * 100.0;
            alignmentString.append("Number of SSE = ").append(n1 + 1).append(
                    "\r\n").append("Number of SSE = ").append(n2 + 1).append(
                            "\r\n").append(alTypes1).append("\r\n").append(
                            alAresultantsSymbols1).append("\r\n").append(
                            alAinterdirectionsSymbols1).append("\r\n").append(
                            alignmentResult).append("\r\n").append(alTypes2).
                    append(
                            "\r\n").append(alAresultantsSymbols2).append("\r\n").
                    append(alAinterdirectionsSymbols2).append("\r\n").append(
                            "Similarity score = ").append(similarityScore).
                    append(
                            "%").append("   ").append("Aligned = ").append(
                            numAlign).
                    append("    ").append("Not aligned = ").append(numNotAlign).
                    append("\r\n").append(" Initial LRMSD = ").append(
                            ProteinWriter.format((double) result.get(2)));
        }
        return SSeMatchs;
    }

    public ArrayList<Object> calculateFinalAlignment(Chain pc1, Chain pc2,
            ArrayList<Integer> SSeMatches, int bestRotatNum, int steps) throws
            Exception
    {
        if (steps == 0)
        {
            steps = 3;
        }
        int SSen1, SSen2;
        SSen1 = pc1.getSseVector().size();
        SSen2 = pc2.getSseVector().size();

        int n1, n2;
        n1 = pc1.getResiduePointCA().size();
        n2 = pc2.getResiduePointCA().size();

        double d0 = (1.24 * Math.pow(Math.min(n1, n2) * 1.0 - 15, 1.0 / 3) - 1.8);
        int numOfAlignSSe = numOfNoneZeros(SSeMatches, -1);
        DoubleMatrix pc1Centroids = new DoubleMatrix(3, numOfAlignSSe);
        DoubleMatrix pc2Centroids = new DoubleMatrix(3, numOfAlignSSe);

        int j = 0;
        for (int i = 0; i < SSen1; i++)
        {
            if (SSeMatches.get(i) != -1)
            {
                pc1Centroids.put(0, j, pc1.getSseVector().get(i).
                        getCenteroid().getX());
                pc1Centroids.put(1, j, pc1.getSseVector().get(i).
                        getCenteroid().getY());
                pc1Centroids.put(2, j, pc1.getSseVector().get(i).
                        getCenteroid().getZ());
                int sseIndex = SSeMatches.get(i);
                pc2Centroids.put(0, j, pc2.getSseVector().get(sseIndex).
                        getCenteroid().getX());
                pc2Centroids.put(1, j, pc2.getSseVector().get(sseIndex).
                        getCenteroid().getY());
                pc2Centroids.put(2, j, pc2.getSseVector().get(sseIndex).
                        getCenteroid().getZ());
                j++;
            }
        }

        ProteinPermutator pp = new ProteinPermutator(new Protein());
        ArrayList<Coordinate> pointsCA = new ArrayList<>();
        for (int i = 0; i < pc1.getResiduePointCA().size(); i++)
        {
            pointsCA.add(pp.rotatedPoints(pc1.getResiduePointCA().get(i),
                    bestRotatNum));
        }
        pc1.setResiduePointCA(pointsCA);

        ArrayList<Object> kabschResult = kabsch(pc1Centroids, pc2Centroids);

        //Kabtch here
        //Result from kabach
        Matrix U = new Matrix(((DoubleMatrix) kabschResult.get(0)).toArray2());
        Matrix R = new Matrix(((DoubleMatrix) kabschResult.get(1)).toArray2());

        //*********
        Matrix fromCoordinatesToMateix = Matrix.fromCoordinatesToMatrix(
                pc1.getResiduePointCA());
        fromCoordinatesToMateix = new Matrix(U.mmul(fromCoordinatesToMateix));// U*pointCA
        fromCoordinatesToMateix = Matrix.add(fromCoordinatesToMateix,
                R.repeat(n1));
        double lrms = (double) kabschResult.get(2);

        pc1.setResiduePointCA(fromCoordinatesToMateix.toCoordinates());
        double tmScore = 0;
        for (int counter = 0; counter < steps; counter++)
        {
            ArrayList<Integer> pc1CAMatches = new ArrayList<>(n1);
            ArrayList<Integer> pc2CAMatches = new ArrayList<>(n2);
            for (int i = 0; i < Math.max(n1, n2); i++)
            {
                if (i < n1)
                {
                    pc1CAMatches.add(0);
                }
                if (i < n2)
                {
                    pc2CAMatches.add(0);
                }
            }

            int numOfAlignedCA = 0;
            ArrayList<Integer> pc1StartGaps = new ArrayList<>(SSen1);
            ArrayList<Integer> pc2StartGaps = new ArrayList<>(SSen2);

            ArrayList<Integer> pc1EndGaps = new ArrayList<>(SSen1);
            ArrayList<Integer> pc2EndGaps = new ArrayList<>(SSen2);

            for (int i = 0; i < SSen1; i++)
            {
                if (SSeMatches.get(i) != -1)
                {
                    int startCA1 = getCAindex(pc1,
                            pc1.getSseVector().get(i).getIntialIndex());
                    int endCA1 = getCAindex(pc1,
                            pc1.getSseVector().get(i).getEndIndex());
                    int startCA2 = getCAindex(pc2, pc2.getSseVector().get(
                            SSeMatches.get(i)).getIntialIndex());
                    int endCA2 = getCAindex(pc2, pc2.getSseVector().get(
                            SSeMatches.get(i)).getEndIndex());

                    int minIndex1 = startCA1;
                    int minIndex2 = startCA2;
                    double minValue = 100;
                    int neighbours;
                    if (pc1.getDescriptor().getTypes().charAt(i) == 'H')
                    {
                        neighbours = 4;
                    } else
                    {
                        neighbours = 3;
                    }
                    int m = endCA1 - startCA1 + 1;
                    int n = endCA2 - startCA2 + 1;
                    Matrix distances = new Matrix(m, n);
                    for (int l = 0; l < m; l++)
                    {
                        for (int k = 0; k < n; k++)
                        {
                            Coordinate v = new Coordinate();
                            Coordinate v1 = pc1.getResiduePointCA().
                                    get(l + startCA1);
                            Coordinate v2 = pc2.getResiduePointCA().
                                    get(k + startCA2);

                            v.substruct(v1, v2);
                            distances.put(l, k, v.squareOfSum());

                        }
                    }
                    distances = distances.sqrt();
                    m = endCA1 - startCA1 + 1 - neighbours + 1;
                    n = endCA2 - startCA2 + 1 - neighbours + 1;
                    double sumOfDistances;
                    for (int o = 0; o < m; o++)
                    {
                        for (int k = 0; k < n; k++)
                        {
                            if (pc1.getDescriptor().getTypes().charAt(i) == 'H')
                            {
                                sumOfDistances = distances.get(o, k) +
                                         distances.get(o + 1, k + 1) +
                                         distances.get(o + 2, k + 2) +
                                         distances.get(o + 3, k + 3);
                            } else
                            {
                                sumOfDistances = distances.get(o, k) +
                                         distances.get(o + 1, k + 1) +
                                         distances.get(o + 2, k + 2);
                            }
                            if (minValue > sumOfDistances)
                            {
                                minValue = sumOfDistances;
                                minIndex1 = o + startCA1;
                                minIndex2 = k + startCA2;
                            }
                        }
                    }
                    setRange(pc1CAMatches, minIndex1, minIndex1 + neighbours - 1,
                            minIndex2);
                    setRange(pc2CAMatches, minIndex2, minIndex2 + neighbours - 1,
                            minIndex1);
                    numOfAlignedCA += neighbours;
                    int left1 = minIndex1 - 1;
                    int right1 = minIndex1 + neighbours;

                    int left2 = minIndex2 - 1;
                    int right2 = minIndex2 + neighbours;

                    while (left1 >= startCA1 && left2 >= startCA2)
                    {
                        pc1CAMatches.set(left1, left2);
                        pc2CAMatches.set(left2, left1);
                        numOfAlignedCA++;
                        left1--;
                        left2--;
                    }

                    while (right1 <= endCA1 && right2 <= endCA2)
                    {
                        pc1CAMatches.set((int) right1, right2);
                        pc2CAMatches.set((int) right2, right1);
                        numOfAlignedCA++;
                        right1++;
                        right2++;
                    }

                    pc1StartGaps.add(right1);
                    pc2StartGaps.add(right2);
                    pc1EndGaps.add(left1);
                    pc2EndGaps.add(left2);

                    if (left1 == 0)
                    {
                        pc1EndGaps.set(i, 1);
                    }
                    if (left2 == 0)
                    {
                        pc2EndGaps.set(i, 1);
                    }
                }
            }

            pc1StartGaps = noneZeros(pc1StartGaps);
            pc1StartGaps.add(0, 0);

            pc2StartGaps = noneZeros(pc2StartGaps);
            pc2StartGaps.add(0, 0);

            pc1EndGaps = noneZeros(pc1EndGaps);
            pc1EndGaps.add(n1 - 1);

            pc2EndGaps = noneZeros(pc2EndGaps);
            pc2EndGaps.add(n2 - 1);

            /**
             * ************ Ahmed ***************
             */
            for (int i = 0; i < pc1StartGaps.size(); i++)
            {
                int startGap1 = pc1StartGaps.get(i);
                int endGap1 = pc1EndGaps.get(i);
                int startGap2 = pc2StartGaps.get(i);
                int endGap2 = pc2EndGaps.get(i);
                if (endGap1 - startGap1 > 0 && endGap2 - startGap2 > 0)
                {
                    int m = (endGap1 - startGap1) + 1;
                    int n = (endGap2 - startGap2) + 1;
                    ArrayList<Coordinate> contactPairs = new ArrayList<>();
                    double distances[][] = new double[m][n];
                    for (int x = 0; x < m; x++)
                    {
                        for (int y = 0; y < n; y++)
                        {
                            double v[] = new double[3];
                            Coordinate c = pc1.getResiduePointCA().get(x +
                                     startGap1);
                            Coordinate c2 = pc2.getResiduePointCA().get(y +
                                     startGap2);
                            v[0] = pc1.getResiduePointCA().get(x + startGap1).
                                    getX() - pc2.getResiduePointCA().get(y +
                                             startGap2).
                                    getX();
                            v[1] = pc1.getResiduePointCA().get(x + startGap1).
                                    getY() - pc2.getResiduePointCA().get(y +
                                             startGap2).
                                    getY();
                            v[2] = pc1.getResiduePointCA().get(x + startGap1).
                                    getZ() - pc2.getResiduePointCA().get(y +
                                             startGap2).
                                    getZ();
                            distances[x][y] = v[0] * v[0] + v[1] * v[1] + v[2] *
                                     v[2];

                            contactPairs.add(new Coordinate(x + startGap1, y +
                                     startGap2, Math.sqrt(distances[x][y])));
                        }
                    }

                    //  Collections.reverse(contactPairs);
                    Collections.sort(contactPairs, new Comparator<Coordinate>()
                    {
                        @Override
                        public int compare(Coordinate o1, Coordinate o2)
                        {
                            return compare(o1.getZ(), o2.getZ());
                        }

                        private int compare(double x, double y)
                        {
                            return Double.compare(x, y);
                        }
                    });

                    int z = 0;
                    int numAlignedContacts = 0;
                    while (z < contactPairs.size() &&
                            contactPairs.get(z).getZ() <=
                             d0 &&
                             numAlignedContacts < m + 1 && numAlignedContacts <
                             n + 1)
                    {
                        int x = (int) contactPairs.get(z).getX();
                        int y = (int) contactPairs.get(z).getY();
                        if (pc1CAMatches.get(x) == 0 && pc2CAMatches.get(y) == 0)
                        {
                            pc1CAMatches.set(x, y);
                            pc2CAMatches.set(y, x);
                            numAlignedContacts++;
                        }
                        z++;
                    }
                    numOfAlignedCA = numOfAlignedCA + numAlignedContacts;
                } else if (endGap1 == startGap1 && endGap2 == startGap2)
                {
                    pc1CAMatches.set(startGap1, startGap2);
                    pc2CAMatches.set(startGap2, startGap1);
                    numOfAlignedCA = numOfAlignedCA + 1;
                }
            }

            if (numOfAlignedCA > 0)
            {
                ArrayList<Object> resFromFind = find(pc1CAMatches);

                numOfAlignedCA = ((ArrayList<Integer>) resFromFind.get(0)).
                        size();
                DoubleMatrix P = Matrix.
                        fromCoordinatesToMatrix(
                                getRange(pc1.getResiduePointCA(),
                                        (ArrayList<Integer>) resFromFind.get(0)));

                DoubleMatrix Q = Matrix.
                        fromCoordinatesToMatrix(
                                getRange(pc2.getResiduePointCA(),
                                        (ArrayList<Integer>) resFromFind.get(1)));
                kabschResult = kabsch(P, Q);

                U = new Matrix((DoubleMatrix) kabschResult.get(0));
                R = new Matrix((DoubleMatrix) kabschResult.get(1));

                //*********
                fromCoordinatesToMateix = Matrix.fromCoordinatesToMatrix(
                        pc1.getResiduePointCA());
                fromCoordinatesToMateix = new Matrix(U.mmul(
                        fromCoordinatesToMateix));// U*pointCA
                fromCoordinatesToMateix = Matrix.add(fromCoordinatesToMateix,
                        R.repeat(n1));
                lrms = (double) kabschResult.get(2);
                pc1.setResiduePointCA(fromCoordinatesToMateix.toCoordinates());

                P = Matrix.
                        fromCoordinatesToMatrix(
                                getRange(pc1.getResiduePointCA(),
                                        (ArrayList<Integer>) resFromFind.get(0)));

                Q = Matrix.
                        fromCoordinatesToMatrix(
                                getRange(pc2.getResiduePointCA(),
                                        (ArrayList<Integer>) resFromFind.get(1)));
                Matrix subResult = new Matrix(P.sub(Q));
                subResult = new Matrix(subResult.getRow(0).
                        mul(subResult.getRow(0)).add(subResult.getRow(1).
                                mul(subResult.getRow(1))).
                        add(subResult.getRow(2).
                                mul(subResult.getRow(2))));
                tmScore = subResult.calcukateTMScore(n1);
            }
        }
        /**
         * By abdallah
         */
        ArrayList<Object> result = new ArrayList<>();
        result.add(tmScore);
        result.add(numOfAlignSSe);
        result.add(U);
        return result;
    }

    private ArrayList<Object> find(ArrayList<Integer> pc1CAMatches)
    {
        ArrayList<Integer> columns = new ArrayList<>();
        ArrayList<Integer> values = new ArrayList<>();
        for (int i = 0; i < pc1CAMatches.size(); i++)
        {
            if (pc1CAMatches.get(i) != 0)
            {
                columns.add(i);
                values.add(pc1CAMatches.get(i));
            }
        }
        ArrayList<Object> result = new ArrayList<>();
        result.add(columns);
        result.add(values);
        return result;
    }

    private ArrayList<Coordinate> getCAPointsAtIndecies(
            ArrayList<Coordinate> coordinates, ArrayList<Integer> integers)
    {
        ArrayList<Coordinate> newCoordinates = new ArrayList<>();
        for (Integer integer : integers)
        {
            if (integer != 0)
            {
                newCoordinates.add(coordinates.get(integer));
            }
        }
        return newCoordinates;
    }

    private int getCAindex(Chain c, int intialIndex)
    {
        for (int j = 0; j < c.getResidueIndex().size(); j++)
        {
            if (c.getResidueIndex().get(j) == intialIndex)
            {
                return j;
            }
        }
        return -1;
    }

    private ArrayList<Integer> noneZeros(ArrayList<Integer> list)
    {
        ArrayList<Integer> temp = new ArrayList();
        for (int elm : list)
        {
            if (elm != 0)
            {
                temp.add(elm);
            }
        }
        return temp;
    }

    private int numOfNoneZeros(ArrayList<Integer> list, int conditionNumber)
    {
        int c = 0;
        for (int is : list)
        {
            if (is != conditionNumber)
            {
                c++;
            }
        }
        return c;
    }

    private void setRange(ArrayList<Integer> arrayList, int start1, int end1,
            int start2)
    {
        for (int i = start1; i <= end1; i++)
        {
            arrayList.add(i, start2);
            start2++;
        }
    }

    private ArrayList<Coordinate> getRange(ArrayList<Coordinate> arrayList,
            ArrayList<Integer> indecisArrayList)
    {
        ArrayList<Coordinate> result = new ArrayList<Coordinate>();
        for (int i = 0; i < indecisArrayList.size(); i++)
        {
            result.add(arrayList.get(indecisArrayList.get(i)));
        }
        return result;
    }

    public ArrayList<Object> kabsch(DoubleMatrix P, DoubleMatrix Q) throws
            Exception
    {
        try
        {
            int D = P.getRows();
            int N = P.getColumns();
            DoubleMatrix m = DoubleMatrix.ones(1, N);
            m = m.div(N);

            DoubleMatrix p0 = P.mmul(m.transpose());
            DoubleMatrix q0 = Q.mmul(m.transpose());
            DoubleMatrix v1 = DoubleMatrix.ones(1, N);
            P = P.sub(p0.mmul(v1));
            Q = Q.sub(q0.mmul(v1));
            DoubleMatrix Pdm = new DoubleMatrix(D, N);

            for (int i = 0; i < N; i++)
            {
                DoubleMatrix vector = P.getColumn(i).mul(m.get(0, i));
                Pdm.putColumn(i, vector);
            }
            DoubleMatrix C = null;

            C = Pdm.mmul(Q.transpose());

            DoubleMatrix[] svd = Singular.fullSVD(C);
            DoubleMatrix V = svd[0];
            DoubleMatrix W = svd[2];
            DoubleMatrix I = DoubleMatrix.eye(D);

            double d = Matrix.determinant(C.toArray2());
            d = Double.parseDouble(ProteinWriter.format(d));
            if (d < 0)
            {
                I.put(I.getRows() - 1, I.getColumns() - 1, -1);
            }

            DoubleMatrix U = W.mmul(I).mmul(V.transpose());

            DoubleMatrix r = q0.sub(U.mmul(p0));
            DoubleMatrix diff = U.mmul(P).sub(Q);

            double lrms;
            DoubleMatrix temp = diff.mul(diff);
            lrms = Math.sqrt((temp.columnSums().sum()) / N);

            ArrayList<Object> result = new ArrayList<>();
            result.add(U);
            result.add(r);
            result.add(lrms);
            return result;
        } catch (Exception ex)
        {
            throw new Exception("Problem in Kabatsh!");
        }
    }
}
